import React, { Component } from 'react';
import {Route, withRouter} from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/login'
import List from './pages/list'
import Projet from './pages/projet'
import Profil from './pages/profil'

class Routes extends Component {
    render() {
        return (
            <div>
                <Route exact path ='/' component={Home}/>
                <Route exact path ='/login' component={Login}/>
                <Route exact path ='/list' component={List}/>
                <Route exact path ='/projet/:id' component={Projet}/>
                <Route exact path ='/profil' component={Profil}/>
            </div>
        );
    }
}

export default withRouter(Routes);
