const url = "https://wedev-back.herokuapp.com/api/";

exports.getAllTask = (id)  => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'tasks/' + id, {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            console.log(error)
        })
});

exports.createTask = (data, id) => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'tasks/create', {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'auth-token': token},
        body: JSON.stringify({
            title: data.title,
            description: data.description,
            delay: data.delay,
            status: data.status,
            sprintId: data.sprintId
        })
    }).then((response) => {
        resolve(response.json())
    }).catch((error) => {
        console.log(error)
    })
});

exports.editTask = (data, id) => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'tasks/' + id, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json', 'auth-token': token},
        body: JSON.stringify({
            title: data.title,
            description: data.description,
            delay: data.delay,
            status: data.status,
            sprintId: data.sprint_ID
        })
    }).then((response) => {
        resolve(response.json())
    }).catch((error) => {
        console.log(error)
    })
});

exports.deleteTask = (idTask) => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'tasks/' + idTask, {
        method: 'DELETE',
        headers: {'auth-token': token}
    }).then((response) => {
        resolve(response.json())
    })
});
