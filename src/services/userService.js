exports.getUsers = () => new Promise((resolve, reject) => {
    fetch('https://wedev-back.herokuapp.com/api/users')
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            reject(error)
        })
});
exports.getUser = (user) => new Promise((resolve,reject) => {
    let token = localStorage.getItem('token');
    fetch('https://wedev-back.herokuapp.com/api/users/' + user , {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            // console.log(response)
            resolve(response.json())
        })
        .catch((error) => {
            reject(error)
        })
});

exports.connectUser = (email, password) => new Promise((resolve) => {
    fetch('https://wedev-back.herokuapp.com/api/users/login', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
        .then((response) => {
            resolve(response.json())
        })
});

exports.createUser = (user) => new Promise((resolve) => {
    fetch('https://wedev-back.herokuapp.com/api/users', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            lastname: user.lastName,
            firstname: user.firstName,
            email: user.email,
            password: user.password,
            phone: user.phone,
            company: user.company,
            siret: user.siret,
            status: user.status,
            profil: user.profil
        })
    })
        .then((response) => {
            resolve(response.json())
        })
});
exports.updateUser = (user, user_id, newPassword = '') => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch('https://wedev-back.herokuapp.com/api/users/'+ user_id, {

        method: 'PUT',
        headers: {'Content-Type': 'application/json','auth-token': token},
        body: JSON.stringify({
            lastname: user.lastName,
            firstname: user.firstName,
            email: user.email,
            password: newPassword === '' ? user.password : newPassword,
            phone: user.phone,
            company: user.company,
            siret: user.siret,
            status: user.status,
            profil: user.profil
        })
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) =>{
        console.log(error)
    })
});

