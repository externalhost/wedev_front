const url = "https://wedev-back.herokuapp.com/api/";


exports.createProject = (project, clientId, userId) => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'project', {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'auth-token': token},
        body: JSON.stringify({
            title: project.title,
            startDate: project.startDate,
            endDate: project.endDate,
            status: project.status,
            requiredTime: project.requiredTime,
            stack: [project.stack],
            totalCost: project.totalCost,
            dailyCost: project.dailyCost,
            clientId: clientId,
            userId: userId,
        })
    }).then((response) => {
        resolve(response.json())
    }).catch((error) => {
        console.log(error)
    })
});

exports.getAllProject = (userId)  => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'project?userId=' + userId, {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            console.log(error)
        })
});


exports.getProject = (id)  => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'project/' + id, {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            console.log(error)
        });
});
