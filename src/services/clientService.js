const url = "https://wedev-back.herokuapp.com/api/";
const token = localStorage.getItem('token');

exports.createClient = (client) => new Promise((resolve) => {
        console.log(client);
        fetch(url + 'clients/create', {
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'auth-token': token},
            body: JSON.stringify({
                company: client.company,
                address: client.address,
                contactLastName: client.contactLastName,
                contactFirstName: client.contactFirstName,
                contactEmail: client.contactEmail,
                contactPhone: client.contactPhone,
            })
        }).then((response) => {
            resolve(response.json())
        })
});

exports.getAllClients = () => new Promise((resolve) => {
    fetch(url + 'clients/getAll', {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            console.log(error)
        })
})
