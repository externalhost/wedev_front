const url = "https://wedev-back.herokuapp.com/api/";

exports.getSprint = (id)  => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'sprints/' + id, {
        method: 'GET',
        headers: {'auth-token': token}
    })
        .then((response) => {
            resolve(response.json())
        })
        .catch((error) => {
            console.log(error)
        })
});

exports.createSprint = (data, id) => new Promise((resolve) => {
    let token = localStorage.getItem('token');
    fetch(url + 'sprints/' + id, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'auth-token': token},
        body: JSON.stringify({                    
            title: data.title,
            startDate: data.startDate,
            endDate: data.endDate,
            status: data.status
        })
    }).then((response) => {
        resolve(response.json())
    }).catch((error) => {
        console.log(error)
    })
});