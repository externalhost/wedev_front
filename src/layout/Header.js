import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import logo from '../assets/img/logo.svg'
const {getIdUser} = require('../middleware');
const  {getUser} = require('../services/userService');
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            showMenu: false,
            userId: getIdUser()
        };
        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    UNSAFE_componentWillMount() {
        if (getIdUser() !== false) {
            getUser(getIdUser()).then((response) => {
                if (response.status === 'ok') {
                    let user = response.data[0];
                    this.setState({
                        userName: user.firstname + ' ' + user.lastname
                    });
                }
            });
        }
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({ showMenu: true }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    closeMenu(event) {

        if (!this.dropdownMenu.contains(event.target)) {

            this.setState({ showMenu: false }, () => {
                document.removeEventListener('click', this.closeMenu);
            });

        }
    }

    disconnect() {
        localStorage.removeItem('token');
        window.location.reload();
    }

    render() {
        return (
            <header className="header-main row">
                <div className="col-xs-4">
                    <Link to="/"><img src={logo} alt="logo" className='logo'/></Link>

                </div>
                <div className="col-xs-8">

                    {!getIdUser() ? (  <ul className="nav__list">
                        <li className="nav__item btn--rd btn btn--light_grey login">
                            <Link to="/login"><p>login</p></Link>
                        </li>
                    </ul>) : (
                        <div className="nav__item">
                            <div className='icone_profil'>
                                <i className="fas fa-user"></i>
                                <p onClick={this.showMenu} >{this.state.userName}</p>
                                {this.state.showMenu ? (
                                        <i className="fas fa-sort-up"> </i>
                                    ) :
                                    <i className="fas fa-sort-down"> </i>
                                }
                            </div>
                            {
                                this.state.showMenu
                                    ? (
                                        <div
                                            className="menu_profil"
                                            ref={(element) => {
                                                this.dropdownMenu = element;
                                            }}
                                        >
                                            <Link to="/list">Projets</Link>
                                            <Link to="/profil">Profil</Link>
                                            <Link onClick={this.disconnect} to="/">Déconnexion</Link>
                                        </div>
                                    )
                                    : null
                            }
                        </div>
                    )}
                </div>
            </header>
        );
    }
}

export default Header;
