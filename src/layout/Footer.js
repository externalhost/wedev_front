import React, { Component } from 'react';
import logo from "../assets/img/logo.svg";

class Footer extends Component {
    render() {
        return (

            <footer>
                <div className="row">
                    <div className="col-xs-4">
                        <img src={logo} alt="logo" className='logo'/>
                    </div>

                    <div className="col-xs-4">
                        <p>COPYRIGHT 2020</p>
                    </div>
                    <div className="col-xs-4">
                    <div className="s-media-container">
                        <ul>
                            <li><i className="fab fa-facebook-f"></i></li>
                            <li><i className="fab fa-twitter"></i></li>
                            <li><i className="fab fa-linkedin-in"></i></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
