import React, { Component } from "react";
import './index.scss'
import { Redirect } from 'react-router-dom'

const {connectUser} = require('../../services/userService');

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            redirect: false,
            error : ''
        }
    }
    handleChange = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };
    submitConnexion = (event) => {
        event.preventDefault();
        connectUser(this.state.email, this.state.password)
            .then((response) => {
                if (response.status === 'ok') {
                    localStorage.setItem('token', response.token);
                    this.setState(
                        {redirect : true}
                    );
                    window.location.reload();
                } else {
                    this.setState(
                        {error : response.message}
                    );
                }
            })
    };
    render() {
        const  disabled = !(this.state.email && this.state.password);
        return (
            <form className="form_login">
                {this.state.redirect ? <Redirect to='/list' /> : '' }
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                <input type="email" name='email' className="form_input" placeholder="Login" value={this.state.email} onChange={this.handleChange}/>

                <input type="password" name='password' className="form_input" placeholder="Password" value={this.state.password} onChange={this.handleChange}/>

                <button type="submit" className="form_button btn btn--green btn--md" disabled={disabled} onClick={this.submitConnexion}>
                    <p>Submit</p>
                </button>
            </form>
        );
    }
}
