import React from 'react';
import './index.scss'

const modal = () => {
    return (
        <div className="modal">
            <i className="fas fa-times cross"></i>
            <div className='header'>
                <h3>title modal</h3>
            </div>
            <div className='content'>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                    the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                    of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
            </div>
            <div className='footer'>
                <button className='btn--black btn--md'><a>Cancel</a></button>
                <button className='btn--green btn--md'><a>Validate</a></button>
            </div>
        </div>
    );
};

export default modal;
