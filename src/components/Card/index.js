import React from 'react';
import './index.scss'

const card = (props) =>{
    return (
        <div className="component_card">
            {props.children}
        </div>
    );
};

export default card;
