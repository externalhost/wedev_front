import React, { Component } from "react";
import './index.scss'
import {Redirect} from "react-router-dom";

const {createUser} = require('../../services/userService');
const securityForm = require('./../../middleware/securityForm');

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                email : '',
                password: '',
                phone: '',
                company: '',
                siret: '',
                status: '',
                profil: '',
            },
            stepOne: true,
            redirect: false,
            error: ''
        }
    }
    handleChange = (event) => {
        this.setState({
            user: {
                ...this.state.user,
                [event.target.name] : event.target.value
            }
        });
    };

    createUser = (event) => {
        event.preventDefault();
        let goodSiret = securityForm.verifSiret(this.state.user.siret);
        if (goodSiret === '') {
            createUser(this.state.user)
                .then((response) => {
                    if (response.status === 'ok') {
                        localStorage.setItem('token', response.token);
                        this.setState(
                            {redirect : true}
                        );
                        window.location.reload();
                    } else {
                        this.setState(
                            {error : response.message}
                        );
                    }
                });
        } else {
            this.setState(
                {error: goodSiret}
            );
        }
    };

    nextStep = (event) => {
        event.preventDefault();
        let goodPhone = securityForm.verifPhone(this.state.user.phone);
        let email = securityForm.verifEmail(this.state.user.email);
        let password = securityForm.verifPassword(this.state.user.password);
        if (goodPhone + email + password === '') {
            this.setState(
                {
                    stepOne: false,
                    error: ''
                }
            );
        } else {
            let errorMessage = goodPhone;
            errorMessage += errorMessage !== '' && email !== '' ? '\n \n' + email : email;
            errorMessage += errorMessage !== '' && password !== '' ? '\n \n' + password : password;
            this.setState(
                {error: errorMessage}
            );
        }
    };

    backStep = () => {
        this.setState(
            {
                stepOne: true
            }
        );
    };

    setClassPart = (ifStepOne) => {
        return ifStepOne === this.state.stepOne ? 'form_part' : 'form_part hidden'
    };

    setClassStep = (ifStepOne) => {
        return ifStepOne === this.state.stepOne ? 'form_step focus' : 'form_step'
    };

    render() {
        const active_button1 = !(
            this.state.user.firstName &&
            this.state.user.lastName &&
            this.state.user.email &&
            this.state.user.password &&
            this.state.user.phone
        );
        const active_button2 = !(
            this.state.user.company &&
            this.state.user.siret &&
            this.state.user.status &&
            this.state.user.profil
        );
        return (
            <form className="form_register">
                {this.state.redirect ? <Redirect to='/list' /> : '' }
                <div className='form_steps'>
                    <div className={this.setClassStep(true)} onClick={this.backStep}><p>1</p></div>
                    <div className={this.setClassStep(false)}><p>2</p></div>
                </div>
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}

                <div className={this.setClassPart(true)}>
                    <input name="firstName" type="text" className="form_input" placeholder="First name" value={this.state.user.firstName} onChange={this.handleChange}/>

                    <input name="lastName" type="text" className="form_input" placeholder="Last name" value={this.state.user.lastName} onChange={this.handleChange}/>

                    <input name="email" type="email" className="form_input" placeholder="Enter email" value={this.state.user.email} onChange={this.handleChange}/>

                    <input name="password" type="password" className="form_input" placeholder="Enter password" value={this.state.user.password} onChange={this.handleChange}/>

                    <input name="phone" type="text" className="form_input" placeholder="Enter Phone" value={this.state.user.phone} onChange={this.handleChange}/>

                    <button type="submit" disabled={active_button1} className="form_button btn btn--green btn--md" onClick={this.nextStep}>
                        <p>Suivant</p>
                    </button>
                </div>

                <div className={this.setClassPart(false)}>
                    <input name="company" type="text" className="form_input" placeholder="Enter Company" value={this.state.user.company} onChange={this.handleChange}/>

                    <input name="siret" type="text" className="form_input" placeholder="Enter siret" value={this.state.user.siret} onChange={this.handleChange}/>

                    <input name="status" type="text" className="form_input" placeholder="Enter Status" value={this.state.user.status} onChange={this.handleChange}/>

                    <input name="profil" type="text" className="form_input" placeholder="Enter Profil" value={this.state.user.profil} onChange={this.handleChange}/>

                    <button type="submit" disabled={active_button2} className="form_button btn btn--green btn--md" onClick={this.createUser}>
                        <p>Sign Up</p>
                    </button>
                </div>
            </form>
        );
    }
}

