import React, {Component} from 'react';
import './index.scss'
import Select from 'react-select'

const {createClient, getAllClients} = require('../../services/clientService');
const {createProject} = require('../../services/projectService');
const {getIdUser} = require('../../middleware');
const securityForm = require('./../../middleware/securityForm');

class FormProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title : '',
            delay: '',
            startDate: '',
            endDate: '',
            stack: '',
            status: '',
            coutHoraire: '',
            budget: '',
            lastName: '',
            firstName: '',
            email: '',
            address: '',
            phone: '',
            company: '',
            etape: 1,
            clients: [],
            valueSelect: '',
            optionClient: true,
            error:  ''
        };

        this.selectStatuts = [
            {value: 'doing', label: 'En cours'},
            {value: 'done', label: 'Réalisé'}
        ]
    }
    handleChange = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };

    closeModal = () => {
        this.props.onChange();
    }

    handleChangeSelect = (event) => {
        this.setState(
            {valueSelect : event.value}
        );
    };

    handleChangeSelectStatus = (event) => {
        this.setState(
            {status : event.value}
        );
    };

    setClassPart = (etape) => {
         return this.state.etape === etape ? 'form_part ' : 'form_part hidden';
    };

    setClassStep = (step) => {
        return this.state.etape === step ? 'form_step focus' : 'form_step';
    };

    setClassClient = () => {

        this.setState(
            {optionClient : !this.state.optionClient}
        );
    };

    changeStep = (step) => {
        let confirmForm = '';
        if (step === 2) {
            confirmForm = securityForm.verifDate(this.state.startDate, this.state.endDate)
        } else if (step === 3) {
            //confirmForm = securityForm.verifEmail(this.state.email);
        }
        if (confirmForm === '') {
            this.setState({
                etape : step,
                error: ''
            })
        } else {
            this.setState({error : confirmForm})
        }
    };

    getClients = () => {
        getAllClients()
            .then((response) => {
                if (response.status === 'ok') {
                    let clients = [];
                    response.data.map(client => clients.push({value: client._id, label: client.contactLastName}))
                    this.setState(
                        {clients : clients}
                    );
                }
            })
    };
    createProject = (newClient) => {
        let client = {
            company: this.state.company,
            address: this.state.address,
            contactLastName: this.state.lastName,
            contactFirstName: this.state.firstName,
            contactEmail: this.state.email,
            contactPhone: this.state.phone,
        };
        let project = {
            title: this.state.title,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            status: this.state.status,
            requiredTime: this.state.delay,
            stack: this.state.stack,
            totalCost: this.state.budget,
            dailyCost: this.state.coutHoraire
        };

        if (newClient) {
            let email = securityForm.verifEmail(this.state.email);
            let phone = securityForm.verifPhone(this.state.phone);
            if (email !== '' || phone !== '') {
                let errorMessage = phone !== '' && email !== '' ? phone + '\n \n' + email : phone + email;
                this.setState({error : errorMessage});
                return;
            }
            createClient(client)
                .then((response) => {
                    if (response.status === 'ok' && getIdUser() !== false) {
                        createProject(project, response.data._id, getIdUser())
                            .then((response) => {
                                if (response.status === 'ok') {
                                    this.closeModal();
                                    console.log('Projet crée')
                                }
                            })
                    }
                })
        } else {
            createProject(project, this.state.valueSelect, getIdUser())
                .then((response) => {
                    if (response.status === 'ok') {
                        this.closeModal();
                        console.log('Projet crée')
                    }
                })
        }
    };

    render() {

        const active_button1 = !(
            this.state.title &&
            this.state.delay &&
            this.state.startDate &&
            this.state.endDate
        );
        const active_button2 = !(
            this.state.status &&
            this.state.stack &&
            this.state.coutHoraire &&
            this.state.budget
        );
        const active_button3 = !(
            this.state.firstName &&
            this.state.lastName &&
            this.state.email &&
            this.state.address &&
            this.state.phone &&
            this.state.company
        );

        return (
            <form className='form_project'>
                <div className="form_steps">
                    <div className={this.setClassStep(1)}><p>1</p></div>
                    <div className={this.setClassStep(2)}><p>2</p></div>
                    <div className={this.setClassStep(3)}><p>3</p></div>
                </div>
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                <div className= {this.setClassPart(1)}>
                    <input type="text" name="title" className="form_input" placeholder="Titre du projet" value={this.state.title} onChange={this.handleChange}/>
                    <input type="text" name='delay' className="form_input" placeholder="Delay" value={this.state.delay} onChange={this.handleChange}/>
                    <input type="date" name="startDate" className="form_input"  placeholder="Date de début" value={this.state.startDate} onChange={this.handleChange}/>
                    <input type="date" name="endDate" className="form_input" placeholder="Date de fin" value={this.state.endDate} onChange={this.handleChange}/>
                    <button disabled={active_button1} type="submit" className="form_button btn btn--green btn--md" onClick={(e) => {e.preventDefault(); this.changeStep(2)}}>
                        <p>Valider</p>
                    </button>
                </div>
                <div className= {this.setClassPart(2)}>
                    <Select  className="form_select" options={this.selectStatuts} onChange={this.handleChangeSelectStatus}/>
                    <input type="text" name="stack" className="form_input" placeholder="stack" value={this.state.stack} onChange={this.handleChange}/>
                    <input type="text" name='coutHoraire' className="form_input" placeholder="Coût Horaire" value={this.state.coutHoraire} onChange={this.handleChange}/>
                    <input type="text" name="budget" className="form_input"  placeholder="Budget" value={this.state.budget} onChange={this.handleChange}/>
                    <button disabled={active_button2} type="submit" className="form_button btn btn--green btn--md" onClick={(e) => {e.preventDefault(); this.changeStep(3); this.getClients();}}>
                        <p>Valider</p>
                    </button>
                </div>
                <div className= {this.setClassPart(3)}>
                    <button className=' form_button btn btn--green btn--md' onClick={(e) => {e.preventDefault(); this.setClassClient()}}>
                        <p> {this.state.optionClient ? 'Créer un client' : 'Choisir Un client'}</p>
                    </button>
                    <div className={!this.state.optionClient ? 'optionClient optionClient_disabeld': 'optionClient'}>
                        <Select  className="form_select" options={this.state.clients} onChange={this.handleChangeSelect}/>
                        <button disabled={this.state.valueSelect === ''} type="submit" className="form_button btn btn--green btn--md" onClick={(e)  => {e.preventDefault(); this.createProject(false)}}>
                            <p>Créer</p>
                        </button>
                    </div>
                    <div className={this.state.optionClient ? 'optionClient optionClient_disabeld': 'optionClient'}>
                        <input type="text" name="firstName" className="form_input" placeholder="First Name" value={this.state.firstName} onChange={this.handleChange} />
                        <input type="text" name="lastName" className="form_input" placeholder="Last Name" value={this.state.lastName} onChange={this.handleChange} />
                        <input type="text" name='email' className="form_input" placeholder="Mail" value={this.state.email} onChange={this.handleChange}/>
                        <input type="text" name='address' className="form_input" placeholder="Adresse" value={this.state.address} onChange={this.handleChange}/>
                        <input type="text" name="phone" className="form_input"  placeholder="Télèphone" value={this.state.phone} onChange={this.handleChange}/>
                        <input type="text" name="company" className="form_input" placeholder="Société" value={this.state.company} onChange={this.handleChange}/>
                        <button  disabled={active_button3} type="submit" className="form_button btn btn--green btn--md" onClick={(e)  => {e.preventDefault(); this.createProject(true)}}>
                            <p>Créer</p>
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}

export default FormProject;
