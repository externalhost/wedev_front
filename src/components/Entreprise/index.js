import React, { Component } from "react";
import './index.scss'

const {updateUser, getUser} = require('../../services/userService');
const {getIdUser} = require('../../middleware/index');
const securityForm = require('./../../middleware/securityForm');

export default class Entreprise extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                email : '',
                password: '',
                phone: '',
                company: '',
                siret: '',
                status: '',
                profil: '',
            },
            error: '',
            validation:''
        }
    }
    handleChange = (event) => {
        this.setState({
            user: {
                ...this.state.user,
                [event.target.name] : event.target.value
            }
        });
    };

    UNSAFE_componentWillMount() {
        getUser(getIdUser()).then(({data}) => {
            let response = data[0];
            this.setState({
                user: {
                    firstName: response.firstname,
                    lastName: response.lastname,
                    email: response.email,
                    password: response.password,
                    phone: response.phone,
                    company: response.company,
                    siret: response.siret,
                    status: response.status,
                    profil: response.profil,
                }
            });
        });
    }
    updateCompany = (event) => {
        event.preventDefault();
        let goodSiret = securityForm.verifSiret(this.state.user.siret);
        if (goodSiret !== '') {
            this.setState(
                {error : goodSiret}
            );
            return;
        }
        updateUser(this.state.user, getIdUser())
            .then((response) => {
                if (response.status === 'ok') {
                    this.setState(
                        {validation:'Vos données on était modifier', error : ''}
                    );
                } else {
                    this.setState(
                        {error : response.message}
                    );
                }
            })
            .catch((error) =>{
                console.log(error)
            })
    };

    render() {
        const disabled = !(
            this.state.user.company &&
            this.state.user.siret &&
            this.state.user.status &&
            this.state.user.profil
        );
        return (
            <form className="form_register">
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                {this.state.validation !== '' ? (
                    <div className="form_validation">
                        <p>{this.state.validation}</p>
                    </div>
                ): null}
                <input name="company" type="text" className="form_input" placeholder="Your Company" value={this.state.user.company} onChange={this.handleChange}/>

                <input name="siret" type="text" className="form_input" placeholder="Your siret" value={this.state.user.siret} onChange={this.handleChange}/>

                <input name="status" type="text" className="form_input" placeholder="Your Status" value={this.state.user.status} onChange={this.handleChange}/>

                <input name="profil" type="text" className="form_input" placeholder="Your Profil" value={this.state.user.profil} onChange={this.handleChange}/>

                <button type="submit" disabled={disabled} className="form_button btn btn--green btn--md" onClick={this.updateCompany}>
                    <p>valider</p>
                </button>
            </form>
        );
    }
}

