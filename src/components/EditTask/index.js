import React, { Component } from "react";
import './index.scss'
import Select from 'react-select'

const {editTask} = require('../../services/taskService');

export default class EditTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            status: '',
            delay: '',
            sprint_ID: '',
            error: ''
        };
        this.selectStatuts = [
            {value: 'todo', label: 'A faire'},
            {value: 'doing', label: 'En cours'},
            {value: 'done', label: 'Terminé'}
        ]
    }

    UNSAFE_componentWillMount() {
        this.setState({
            title: this.props.task.title,
            description: this.props.task.description,
            status: this.props.task.status,
            delay: this.props.task.delay,
            sprint_ID: this.props.task.sprint_ID
        })
    }

    handleChange = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };

    handleChangeSelectStatus = (event) => {
        this.setState(
            {status : event.value}
        );
    };
    updateTask = (event) => {
        event.preventDefault();
        editTask(this.state, this.props.task._id).then((response) => {
            if (response.status === 'ok') {
                this.props.onChange();
            } else {
                this.setState(
                    {error : response.message}
                );
            }
        })
    };

    render() {
        const  disabled = !(this.state.title && this.state.description && this.state.delay && this.state.status);
        return (
            <form className="form_task">
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                <input type="text" name="title" className="form_input" placeholder="Titre de la Task" value={this.state.title} onChange={this.handleChange}/>
                <input type="text" name="description" className="form_input"  placeholder="Description" value={this.state.description} onChange={this.handleChange}/>
                <input type="number" name="delay" className="form_input" placeholder="Temps de réalisation en heure" value={this.state.delay} onChange={this.handleChange}/>
                <Select value={this.selectStatuts.find(op => {return op.value === this.state.status})} className="form_select" options={this.selectStatuts} onChange={this.handleChangeSelectStatus} selectedValue={this.state.status}/>
                <button type="submit" className="form_button btn btn--green btn--md" disabled={disabled} onClick={this.updateTask}>
                    <p>Update Task</p>
                </button>
            </form>
        );
    }
}
