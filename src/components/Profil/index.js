import React, { Component } from "react";
import './index.scss'

const {updateUser, getUser} = require('../../services/userService');
const {getIdUser} = require('../../middleware/index');
const securityForm = require('./../../middleware/securityForm');
//
export default class Profils extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                email : '',
                password: '',
                phone: '',
                company: '',
                siret: '',
                status: '',
                profil: '',
            },
            newPassword: '',
            error: '',
            validation: ''
        }
    }
    handleChange = (event) => {
        this.setState({
            user: {
                ...this.state.user,
                [event.target.name] : event.target.value
            }
        });
    };

    handleChangePassword = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };

    updateUser = (event) => {
        event.preventDefault();
        let goodPhone = securityForm.verifPhone(this.state.user.phone);
        let password = this.state.newPassword !== '' ? securityForm.verifPassword(this.state.newPassword) : '';
        if (goodPhone !== '' || password !== '') {
            let errorMessage = goodPhone !== '' && password !== '' ?
                goodPhone + '\n \n' + password
                : goodPhone + password;
            this.setState(
                {error : errorMessage, validation:''}
            );
            return;
        }
        updateUser(this.state.user, getIdUser(), this.state.newPassword)
            .then((response) => {
                if (response.status === 'ok') {
                    this.setState(
                        {validation:'Vos données on était modifier', error : ''}
                    );
                } else {
                    this.setState(
                        {error : response.message}
                    );
                }
            })
            .catch((error) =>{
                console.log(error)
            })
    };


    UNSAFE_componentWillMount() {
        if (getIdUser() !== false) {
            getUser(getIdUser()).then(({data}) => {
                let response = data[0];
                this.setState({
                    user: {
                        firstName: response.firstname,
                        lastName: response.lastname,
                        email: response.email,
                        password: response.password,
                        phone: response.phone,
                        company: response.company,
                        siret: response.siret,
                        status: response.status,
                        profil: response.profil,
                    }
                });
            });
        }
    }

    render() {
        const disabled = !(
            this.state.user.firstName &&
            this.state.user.lastName &&
            this.state.user.phone
        );

        return (
            <form className="form_register">
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                {this.state.validation !== '' ? (
                    <div className="form_validation">
                        <p>{this.state.validation}</p>
                    </div>
                ): null}
                    <input name="firstName" type="text" className="form_input" placeholder={this.state.user.firstName} value={this.state.user.firstName} onChange={this.handleChange}/>

                    <input name="lastName" type="text" className="form_input" placeholder="Last name" value={this.state.user.lastName} onChange={this.handleChange}/>

                    <input name="newPassword" type="password" className="form_input" placeholder="New password" value={this.state.newPassword} onChange={this.handleChangePassword}/>

                    <input name="phone" type="text" className="form_input" placeholder="Your Phone" value={this.state.user.phone} onChange={this.handleChange}/>

                    <button type="submit" disabled={disabled} className="form_button btn btn--green btn--md" onClick={this.updateUser}>
                        <p>Valider</p>
                    </button>

            </form>
        );
    }
}

