import React, { Component } from "react";
import './index.scss'
import Select from 'react-select'

const {createSprint} = require('../../services/sprintService');
const securityForm = require('./../../middleware/securityForm');

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            status: '',
            startDate: '',
            endDate: 'false',
            error:''
        }

        this.selectStatuts = [
            {value: 'a faire', label: 'A faire'},
            {value: 'En cours', label: 'En cours'},
            {value: 'terminé', label: 'Terminé'}
        ]
    }
    handleChange = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };
    closeModal = () => {
        this.props.onChange();
        this.setState({
            title: '',
            status: '',
            startDate: '',
            endDate: 'false',
        })
    }
    handleChangeSelectStatus = (event) => {
        this.setState(
            {status : event.value}
        );
    };
    createSprint = (event) => {
        event.preventDefault();
        let confirmForm = securityForm.verifDate(this.state.startDate, this.state.endDate);
        if (confirmForm !== '') {
            this.setState({error : confirmForm})
            return;
        }

        //create Sprint
        createSprint(this.state, this.props.projectId).then((responce) => {
            if(responce.status === 'ok') {
                this.closeModal()
            } else {
                this.setState({error : responce.message})
            }
        });

    };
    render() {
        const  disabled = !(this.state.title && this.state.startDate && this.state.endDate && this.state.status);
        return (
            <form className="form_sprint">
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                <input type="text" name="title" className="form_input" placeholder="Titre du Sprint" value={this.state.title} onChange={this.handleChange}/>
                <input type="date" name="startDate" className="form_input"  placeholder="Date de début" value={this.state.startDate} onChange={this.handleChange}/>
                <input type="date" name="endDate" className="form_input" placeholder="Date de fin" value={this.state.endDate} onChange={this.handleChange}/>
                <Select  className="form_select" options={this.selectStatuts} onChange={this.handleChangeSelectStatus}/>
                <button type="submit" className="form_button btn btn--green btn--md" disabled={disabled} onClick={(event) => this.createSprint(event)}>
                    <p>Create Sprints</p>
                </button>
            </form>
        );
    }
}
