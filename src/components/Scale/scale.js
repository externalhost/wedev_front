import React, {Component} from 'react';

let _this = this;

this.screenHeight = 375;
this.screenWidth = 667;
this.zoomValue = 1;
this.rootWidth = _this.screenWidth;
this.rootHeight = _this.screenHeight;

this.minHeight = 375;//hauteur minimum autorisée de l'animation
this.minWidth = 667;//hauteur minimum autorisée de l'animation
// this.maxWidth = 1481;//hauteur minimum autorisée de l'animation
// this.maxHeight = 832;//hauteur maximum autorisée


this.__window_height = 0;
this.__window_width = 0;


this.resize = function(){
    var b = $("body");

    //ajout des classes
    if(window.width <= 640){
        $("body").addClass("res_640");
    }else{
        $("body").removeClass("res_640");
    }

    if(window.width  > 780){
        $("body").addClass("res_hd");
    }

    //Ajustement du zoom
    //$("#mainContainer").css("width","100%").css("height","100%");
    //var mainContainerH = window.height;
    //var mainContainerW = $("#mainContainer").innerWidth();

    var mainContainerH = window.height;
    var mainContainerW = window.width ;


    if(mainContainerW > window.width) mainContainerW = window.width;

    if(window.height < _this.minHeight){
        // console.log('la 1')
        mainContainerH = _this.minHeight;
        _this.zoomValue = _this.minHeight / _this.screenHeight;
        _this.rootHeight = _this.minHeight / _this.zoomValue;
        _this.rootWidth = mainContainerW / _this.zoomValue;
    }else if(window.height > _this.maxHeight){
        // console.log('la 2')
        mainContainerH = _this.maxHeight;
        _this.zoomValue = _this.maxHeight / _this.screenHeight;
        _this.rootHeight = _this.maxHeight / _this.zoomValue;
        _this.rootWidth = mainContainerW / _this.zoomValue;
    }else{
        // console.log('la 3')
        _this.zoomValue = window.height / _this.screenHeight;
        _this.rootHeight = window.height / _this.zoomValue;
        _this.rootWidth = mainContainerW / _this.zoomValue;
    }

    if(_this.rootWidth < _this.screenWidth){
        // console.log('la 4')
        _this.rootWidth = _this.screenWidth;
        _this.rootHeight = _this.screenHeight;
        _this.zoomValue = window.width/_this.rootWidth;
        mainContainerH = mainContainerW * (_this.screenHeight/_this.screenWidth);
    }
    if(mainContainerW === 375 && mainContainerH <= 667){

        $("#mainContainer").css("width","375px").css("height",_this.screenHeight+"px");
        $("#root").css("width",_this.rootWidth + "px").css("height",_this.rootHeight + "px");

    }else{
        $("#mainContainer").css("width",mainContainerW + "px").css("height",mainContainerH +"px");
        $("#root").css("width",667 + "px").css("height",_this.rootHeight + "px");
    }
    if ((_this.zoomValue * mainContainerW) > 667) {
        var taille = (mainContainerW - (_this.zoomValue * 667))/2
        // console.log(taille + 'aze')
        $("#mainContainer").css('margin-left',''+taille+'px')
    }

    if( ($("#mainContainer").height()) < ($(window).height())){
        console.log('bla')
        var taille = (($(window).height())/2) - ($("#mainContainer").height()/2);
        console.log(taille)
        $("#mainContainer").css('margin-top',''+taille+'px')
        // $("#mainContainer").css('margin-top','calc(50% )')
    }else {
        $("#mainContainer").css('margin-top','0%')

    }

    if ($(window).height() < 375){

        _this.zoomValue = window.height / _this.screenHeight;
        if ($(window).width() <= 568) {
            // console.log(taille + 'aze')
            $("#mainContainer").css('margin-left','calc(50% - ' +333.5 * _this.zoomValue +'px)')
        }
    }



};

$(document).bind('CORE_SIZE_CHANGED',function(){
    _this.resize();

});
window.addEventListener("orientationchange", function() {
    _this.resize();
});

export default Scale;