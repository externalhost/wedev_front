import React, { Component } from "react";
import './index.scss'
import Select from 'react-select'

const {createTask} = require('../../services/taskService');

export default class FormTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sprintId: this.props.sprintId,
            title: '',
            description: '',
            status: '',
            delay: '',
            error: ''
        };

        this.selectStatuts = [
            {value: 'todo', label: 'A faire'},
            {value: 'doing', label: 'En cours'},
            {value: 'done', label: 'Terminé'}
        ]
    }
    handleChange = (event) => {
        this.setState(
            {[event.target.name] : event.target.value}
        );
    };
    closeModal = () => {
        this.props.onChange();
        this.setState(
            {title: '',
            description: '',
            status: '',
            delay: '',}
        );
    };
    handleChangeSelectStatus = (event) => {
        this.setState(
            {status : event.value}
        );
    };
    creatTask = (event) => {
        event.preventDefault();
        //create Task
        createTask(this.state).then( (response) => {
            if(response.status === 'ok') {
                this.closeModal();
            } else {
                this.setState(
                    {error : response.message}
                );
            }
        })
    };
    render() {
        const  disabled = !(this.state.title && this.state.description && this.state.delay && this.state.status);
        return (
            <form className="form_task">
                {this.state.error !== '' ? (
                    <div className="form_error">
                        <p>{this.state.error}</p>
                    </div>
                ): null}
                <input type="text" name="title" className="form_input" placeholder="Titre de la Task" value={this.state.title} onChange={this.handleChange}/>
                <input type="text" name="description" className="form_input"  placeholder="Description" value={this.state.description} onChange={this.handleChange}/>
                <input type="number" name="delay" className="form_input" placeholder="Temps de réalisation en heure" value={this.state.delay} onChange={this.handleChange}/>
                <Select  className="form_select" options={this.selectStatuts} onChange={this.handleChangeSelectStatus}/>
                <button type="submit" className="form_button btn btn--green btn--md" disabled={disabled} onClick={this.creatTask}>
                    <p>Create Task</p>
                </button>
            </form>
        );
    }
}
