import React from "react";
import Header from './layout/Header';
import Footer from './layout/Footer';
import Routes from './Routes'

function App() {

    return (
        <div className="App">
            <Header/>
                <Routes/>
            <Footer/>
        </div>
    );
}

export default App;
