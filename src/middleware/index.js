const jwt = require('jsonwebtoken');

exports.getIdUser =  () => {
    try {
        let token = localStorage.getItem('token');
        let decoded = jwt.verify(token, 'frP95F$cdE75!E');

        let timeToken = new Date(decoded.iat * 1000);
        let timeNow = new Date();
        timeToken.setHours(timeToken.getHours() + 24);

        if (timeNow.getTime() > timeToken.getTime()) {
            localStorage.removeItem('token');
            return false
        }
        return decoded._id
    } catch(err) {
        return false;
    }
};
