exports.verifPhone = (numPhone) => {
    var regex = /\d{10}/;
    if (regex.test(numPhone)) {
        return '';
    } else {
        return 'Le numéro de téléphone doit être composé de 10 chiffres';
    }
};

exports.verifSiret = (numSiret) => {
    var regex = /\d{10}/;
    if (regex.test(numSiret)) {
        return '';
    } else {
        return 'Votre numéro de siret doit être composé de 14 chiffres';
    }
};

exports.verifDate = (startDate, endDate) => {
    let start = new Date(startDate);
    let end = new Date(endDate);
    if (start.getTime() < end.getTime()) {
        return '';
    } else {
        start = start.getDate() + '-' + start.getMonth() + '-' + start.getFullYear();
        end = end.getDate() + '-' + end.getMonth() + '-' + end.getFullYear();
        return 'La date de départ ' + start + ' doit être inférieur a ' + end;
    }
};

exports.verifEmail = (email) => {
    const regex = /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/;
    if (regex.test(email)) {
        return '';
    } else {
        return 'Le format de l\'adresse email n\'est pas valide';
    }
};

exports.verifPassword = (password) => {
    let regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(\w{8,15})$/
    if (regex.test(password)) {
        return''
    } else {
        return 'le password doit être composer d\'au moins une majusccule, une minuscule, un chiffre et doit faire' +
            ' au moins 8 caractère'
    }
};
