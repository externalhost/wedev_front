import React, { Component } from 'react';
import {Link} from "react-router-dom";
import icon1 from '../assets/img/icon1.svg'
import icon2 from '../assets/img/icon2.svg'
import icon3 from '../assets/img/icon3.svg'
import icon4 from '../assets/img/icon4.svg'

class Home extends Component {
    render() {
        return (
            <div>
                <div className="home--accueil ">
                    <div className="center">
                        <h1>
                            Où que vous soyez<br/>
                            Restez <span className='bold'>Freelance</span>

                        </h1>
                        <div className="btn btn--rd btn--blue floatLeft">
                            <Link to="/login">
                            <p className='bold'>
                                INSCRIVEZ-VOUS
                            </p>
                            </Link>
                        </div>
                    </div>
                    <div className="s-media-container">
                        <ul>
                            <li><i className="fab fa-facebook-f"></i></li>
                            <li><i className="fab fa-twitter"></i></li>
                            <li><i className="fab fa-linkedin-in"></i></li>
                        </ul>
                    </div>
                    <div className="btn btn--rd btn--white btn--explore">

                        <Link to="/login">
                            <p className='bold'>
                                EXPLORER
                            </p><i className="fas fa-angle-down"></i>
                        </Link>
                    </div>
                </div>
                <div className="a-propos row">
                    <div className="col-xs-12">
                        <div className="btn btn--white btn--lg">
                            <p className='bold'>Voici WeDev</p>
                        </div>
                    </div>

                    <div className="col-xs-12">
                    <h2 className='bold'>Application de gestion<br/>
                        de projet pour les dev</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore<br/>et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris<br/>nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                    <div className='col-xs-3'>
                        <img src={icon1} alt=""/>
                        <p>Vos reporting <br/> centralisés</p>
                    </div>
                    <div className='col-xs-3'>
                        <img src={icon2} alt=""/>
                        <p>Le suivi de vos activités<br/>
                            en temps réel</p>
                    </div>
                    <div className='col-xs-3'>
                        <img src={icon3} alt=""/>
                        <p>Un gain de temps<br/>
                            pour le pilotage</p>
                    </div>
                    <div className='col-xs-3'>
                        <img src={icon4} alt=""/>
                        <p>Des  utilitaires<br/>
                            pour votre dev</p>
                    </div>
                </div>
                <div className="comment">
                    <div className="row">
                        <div className="col-xs-6">
                            <div className="centrer center-up">
                            <div className="title">
                                <p className="subtitle">Comment ça marche ?</p>
                                <h2>Tous vos outils de dev<br></br> réuni sur une <br></br>seule application</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className="point">
                                <div className="bullet"><p>1</p></div>
                                <div className="little-tittle"><p>Créer vos projets</p></div>
                                <div className="text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </div>
                            </div>
                            <div className="point">
                                <div className="bullet"><p>2</p></div>
                                <div className="little-tittle"><p>Ajouter vos sprints</p></div>
                                <div className="text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </div>
                            </div>
                            <div className="point">
                                <div className="bullet"><p>3</p></div>
                                <div className="little-tittle"><p>Affecter vos tâches</p></div>
                                <div className="text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="offre">
                    <div className="modal">
                        <div className="title bold">
                            <h3>OFFRE DE LANCEMENT</h3>
                        </div>
                        <p className="text">
                            inscrivez-vous et recevez prochainement<br/>
                            un accès premium à l’application WeAreData
                        </p>
                        <div className="btn btn--md btn--green">JE M’INSCRIS</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
