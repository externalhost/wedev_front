import React, {Component} from 'react';
import Connexion from '../components/Login'
import SignUp from '../components/Signup'
import Card from '../components/Card'
import {Redirect} from "react-router-dom";
const {getIdUser} = require('../middleware');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true
        }
    }
    changeForm = (login) => {
        this.setState(
            {register: !login}
        );
    };
    setClass = (ifRegister) => {
        return ifRegister === this.state.register ? 'button_login focus' : 'button_login'
    };

    render() {
        return (
            <div className="height_test">
                {getIdUser() !== false ? <Redirect to='/list'/> : ""}
                <Card>
                    <div className="inscription_connexion">
                        <div className= "header">
                            <div className={this.setClass(true)} onClick={() => this.changeForm(false)}>
                                <p>Inscription</p>
                            </div>
                            <div className={this.setClass(false)} onClick={() => this.changeForm(true)}>
                                <p>Connexion</p>
                            </div>
                        </div>
                        <div className="content">
                            {this.state.register ? <SignUp/> : <Connexion/>}
                        </div>
                    </div>
                </Card>
            </div>
        );
    }
}

export default Login;
