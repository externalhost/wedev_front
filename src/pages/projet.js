import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';
import Card from '../components/Card';
import FormSprint from "../components/FormSprint";
import FormTask from "../components/FormTask";
import EditTask from "../components/EditTask";

import { Draggable, Droppable, DragDropContext } from 'react-beautiful-dnd'

const {getIdUser} = require('../middleware');
const {getProject} = require('../services/projectService');
const {getSprint} = require('../services/sprintService');
const {getAllTask, deleteTask, editTask} = require('../services/taskService');

class Projet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            idUser: getIdUser(),
            projectId: this.props.match.params.id,
            display : false,
            formSprint: 'creatSprint',
            project : [],
            sprints: [],
            updated: false,
            sprintIdForFuturTask: '',
            editTask: ''
        }
    }

    refreshProject = () => {
        getProject(this.state.projectId).then( ({data}) => {
            this.setState({
                projet: data
            });
        });
        getSprint(this.state.projectId).then( ({data}) => {
            // console.log(data);
            this.setState({
                sprints: data
            });
            data.map((sprint, i) => getAllTask(sprint._id).then(({data}) => {
                this.state.sprints[i] = {...this.state.sprints[i], tasks: data}
                this.setState({
                    updated: true
                })
            }));
        });
    }

    UNSAFE_componentWillMount() {
        this.refreshProject();
    }

    deleteTask = (idTask) => {
        deleteTask(idTask).then((response) => {
            if (response.status === 'ok' ) {
                this.refreshProject();
            }
        })
    };

    displayModal = (display) => {
        this.setState(
            {display : display}
        );
    };

    handleChange = () => {
        this.displayModal(!this.state.display);
        this.refreshProject();
    }

    chooseModal = (formSprint) => {
        !this.state.display && this.displayModal(true);
        this.setState(
            {formSprint : formSprint}
        );
    };
    onDragEnd = result => {
        const {draggableId, source, destination} = result;
        let newState = [...this.state.sprints];
        newState.map((sprint, sprintIndex) => {
            if(sprint._id === source.droppableId){
                sprint.tasks.map((task, taskIndex) => {
                    if(task._id === draggableId){
                        editTask({
                            title: task.title,
                            description: task.description,
                            delay: task.delay,
                            status: task.status,
                            sprint_ID: destination.droppableId
                        },task._id);
                        newState.find((el, i) => (el._id === destination.droppableId)).tasks.push(task);
                        newState[sprintIndex].tasks.splice(taskIndex, 1);
                    }
                })
            }
            this.setState(newState);
        })
    }

    render() {
        return (
            <div className="height_test">
                <div className={this.state.display ? 'fond ' : 'fond hidden'}> </div>
                <div className={this.state.display ? 'modal_ ' : 'modal_ hidden'}>
                    <Card>
                        <i className="fas fa-times cross" onClick={() => {this.displayModal(false)}}> </i>
                        <div className="titre">
                            {{
                                creatSprint: <h3>Création de Sprint</h3>,
                                creatTask:  <h3>Création de Task</h3>,
                                updateTask: <h3>Modifier une Task</h3>
                            }[this.state.formSprint]}
                        </div>
                        <div className="content">
                            {{
                                creatSprint:  <FormSprint onChange={this.handleChange} projectId={this.state.projectId}/>,
                                creatTask:   <FormTask onChange={this.handleChange} sprintId={this.state.sprintIdForFuturTask} />,
                                updateTask: <EditTask onChange={this.handleChange} task={this.state.editTask} />
                            }[this.state.formSprint]}
                        </div>
                    </Card>
                </div>
                <div className='create_spring'>
                    <h3>Create Spring</h3>
                    <div className="circle" onClick={() => {this.chooseModal('creatSprint')}}>
                        <p>+</p>
                    </div>
                </div>

                <div className="containeur_colone">
                    <DragDropContext onDragEnd={this.onDragEnd}>
                    {this.state.sprints.length === 0 ? (
                        <div className="container_width">
                            <div className="colone-add">
                                <div className="circle" onClick={() => {this.chooseModal('creatTask')}}>
                                    <p>+</p>
                                </div>
                            </div>
                        </div>
                    ) : (
                    <div className="container_width">
                        {this.state.sprints.length > 0 && this.state.sprints.map(sprint => {
                            return (
                                <Droppable droppableId={sprint._id}>
                                    {(provided, snapshot) => (
                                        <div className="colone"
                                            ref={provided.innerRef}
                                            innerRef={provided.innerRef}
                                            {...provided.droppableProps}
                                        >
                                            <h3>{sprint.title}</h3>
                                            {this.state.updated && sprint.tasks && sprint.tasks.map((task, i) => {
                                                return (
                                                    <Draggable draggableId={task._id}>
                                                        {(provided, snapshot) => (
                                                            <div ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}
                                                                innerRef={provided.innerRef}
                                                                isDragging={snapshot.isDragging}
                                                                className="card little">
                                                                <p className='card_titre'>{task.title}</p>
                                                                <p className='card_desc'>{task.description}</p>
                                                                <p className='card_stat'>{task.status}</p>
                                                                <i className="fas fa-times cross" onClick={e => this.deleteTask(task._id)}> </i>
                                                                <i className="pencil fas fa-pencil-alt" onClick={() => {this.setState({editTask: task}); this.chooseModal('updateTask')}}> </i>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                )
                                            })}
                                            <div className="card little add">
                                                <div className="circle" onClick={() => {this.chooseModal('creatTask'); this.setState({sprintIdForFuturTask: sprint._id})}}>
                                                    <p>+</p>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                            </Droppable>
                            )
                        })}
                        <div className="colone-add">
                            <div className="circle" onClick={() => {this.chooseModal('creatSprint')}}>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                    )}
                    <div className="clear"></div>
                    </DragDropContext>
                </div>
            </div>
        );
    }
}

export default Projet;
