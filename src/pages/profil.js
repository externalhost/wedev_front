import React, {Component} from 'react';
import Card from "../components/Card";
import Profils from "../components/Profil";
import Entreprise from "../components/Entreprise";
import {Redirect} from "react-router-dom";
const {getIdUser} = require('../middleware');

class Profil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true
        }
    }
    changeForm = (profils) => {
        this.setState(
            {register: !profils}
        );
    };
    setClass = (ifRegister) => {
        return ifRegister === this.state.register ? 'button_login focus' : 'button_login'
    };

    render() {
        return (
            <div className="height_test profil-container">
                {getIdUser() === false ? <Redirect to='/login'/> : ""}
                <Card>
                    <div className="inscription_connexion">
                        <div className= "header">
                            <div className={this.setClass(true)} onClick={() => this.changeForm(false)}>
                                <p>Moi</p>
                            </div>
                            <div className={this.setClass(false)} onClick={() => this.changeForm(true)}>
                                <p>Entreprise</p>
                            </div>
                        </div>
                        <div className="content">
                            {this.state.register ? <Profils/> : <Entreprise/>}
                        </div>
                    </div>
                </Card>
            </div>
        );
    }
}

export default Profil;
