import React, {Component} from 'react';
import { Redirect } from 'react-router-dom'
import Card from '../components/Card'
import FormProject from '../components/FormProject'

const {getIdUser} = require('../middleware');
const {getAllProject} = require('../services/projectService');


class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idUser: getIdUser(),
            display : false,
            project : []
        }
    }

    displayModal = (display) => {
        this.setState(
            {display : display}
        );
    };

    handleChange = () => {
        this.displayModal(!this.state.display);
        this.displayAllProject();
    }

    displayAllProject = () => {
        getAllProject(this.state.idUser)
            .then((response) => {
                if (response.status === 'ok') {
                    this.setState(
                        {project : response.data}
                    );
                }
            })
    };

    UNSAFE_componentWillMount () {
        this.displayAllProject();
    }

    render() {
        return (
            <div className="height_test dash_back">
                {this.state.idUser === false ? <Redirect to='/login'/> : ""}

                <div className={this.state.display ? 'fond ' : 'fond hidden'}> </div>
                <div className={this.state.display ? 'modal_ ' : 'modal_ hidden'}>

                    <Card>
                        <i className="fas fa-times cross" onClick={() => {this.displayModal(false)}}> </i>
                        <div className="titre">
                            <h3>Création de projet</h3>
                        </div>
                        <div className="content">
                            <FormProject onChange={this.handleChange}/>
                        </div>
                    </Card>
                </div>
               <div className="row row_first">
                   <div className="col-xs-3">
                       <div className="card">
                           <div className="circle" onClick={() => {this.displayModal(true)}}>
                               <p>+</p>
                           </div>
                       </div>
                   </div>
                   {this.state.project.map(projet =>
                       <div className="col-xs-3 project" key={projet._id} onClick={() => {this.props.history.push('/projet/' + projet._id)}}>
                           <div className="card">
                               <div className="tile-card">
                                   <h3>{projet.title}</h3>
                               </div>
                               <div className="statut">
                                   <p>{projet.status}</p>
                               </div>
                               <div className="timeline">
                                   <p>{projet.startDate}</p>
                                   <p>{projet.endDate}</p>
                               </div>
                           </div>
                       </div>
                   )}
               </div>
            </div>
        );
    }
}

export default List;
