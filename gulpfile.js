const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('sass',function(){
    return gulp.src('./src/assets/sass/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./src/assets/css'))
});

gulp.task('watch',function () {
    gulp.watch('./src/assets/sass/**/*scss',gulp.series('sass'));
})